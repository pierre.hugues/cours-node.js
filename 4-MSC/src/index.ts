import express, { Request, Response } from "express";

import { defaultRouter } from "./api/default.controler";

const app = express();

app.use(express.json());
app.use("", defaultRouter);

app.listen(3000, () => {
    console.log("SERVER RUNNING ON PORT 3000");
});