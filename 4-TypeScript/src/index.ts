import express, { Request, Response } from "express";

import { Body } from "./interfaces/body.interface";

const app = express();
const router = express.Router();

router.get("/", (req: Request, res: Response) => {
    try {
        const value: string = "3";
        return res.status(200).send(value);
    } catch(error: any) {
        console.error(error);
        return res.status(500).send(error.message);
    }
});

router.post("/body", (req: Request, res: Response) => {
    try {
        const body: Body = req.body;
        return res.status(200).send(`${body.val1}`);
    } catch(error: any) {
        console.error(error);
        return res.status(500).send(error.message);
    }
});

app.use(express.json());
app.use(router);

app.listen(3000, () => {
    console.log("SERVER RUNNING ON PORT 3000");
});