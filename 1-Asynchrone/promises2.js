async function functionPromise(var1, var2) {
    return var1 + var2;
}

async function functionPromise2(var1, var2) {
    throw Error("Error");
}

async function caller() {
    try {
        const result = await functionPromise(1, 2);
        console.log(result);
        await functionPromise2(1, 2);
    } catch(error) {
        console.error(error);
    }
}

caller();