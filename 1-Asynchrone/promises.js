function functionPromise(var1, var2) {
    return new Promise((resolve, reject) => {
        resolve(var1 + var2);
    });
}

function functionPromise2(var1, var2) {
    return new Promise((resolve, reject) => {
        reject("Error");
    });
}

functionPromise(1, 2).then((result) => {
    console.log("--------------------------");
    console.log(result);
});

functionPromise2(1, 2).catch((error) => {
    console.log("--------------------------")
    console.error(error);
});

functionPromise(1, 2).then((result) => {
    console.log("--------------------------")
    console.log(result);
    return functionPromise2(result, 3);
}).catch((error) => {
    console.error(error);
});