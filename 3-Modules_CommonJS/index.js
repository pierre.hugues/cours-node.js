const express = require("express");
const utils = require("./utils");

const app = express();
const router = express.Router();

router.get("/file", async (req, res) => {
    try {
        const fileContent = await utils.getFiles();
        return res.status(200).send(fileContent);
    } catch(error) {
        console.error(error);
        return res.status(500).send(error.message);
    }
});

app.use(express.json());
app.use(router);

app.listen(3000, () => {
    console.log("SERVER RUNNING ON PORT 3000");
});