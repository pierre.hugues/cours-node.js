const fs = require("fs").promises;
const path = require("path");

module.exports = {
    async getFiles() {
        return await fs.readFile(path.join(__dirname, "./file.txt"), "utf-8");
    }
}